package com.agiletestingalliance;
import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {
    @Test
    public void testCompare() throws Exception {

        int k= new MinMax().compare(3,5);
        assertEquals("Case number2 > number1", 5, k);
        
    }

    @Test
    public void testCompare2() throws Exception {

        int k= new MinMax().compare(7,3);
        assertEquals("Case number1 > number2", 7, k);
        
    }

    @Test
    public void testBar() throws Exception {

        String k= new MinMax().bar("Hello");
        assertEquals("Not Null/Empty", "Hello", k);
        
    }

    @Test
    public void testBar2() throws Exception {

        String k= new MinMax().bar("");
        assertEquals("Null/Empty", "", k);
        
    }

}
